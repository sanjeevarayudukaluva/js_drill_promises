/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoards = require("./callback1.cjs");
const getCards = require("./callback2.cjs");
const getLists = require("./callback3.cjs");

function getData(boards, lists, cards, boardId) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let result = {};
      getBoards(boards, boardId)
        .then((data) => {
          
          result["boardsData"] = data;
          return getLists(lists, data.id);
        })
        .then((data) => {
          result["listData"] = data;

          let arrayOfPromises = data.map((element) => {
            return getCards(cards, element.id);
          });
        
          return Promise.allSettled(arrayOfPromises);
        })
        .then((data) => {
          result["cardsData"] = data;
          // console.log(data)
          resolve(result);
        })

        .catch((error) => {
          reject(error);
        });
    }, 2000);
  });
}

module.exports = getData;
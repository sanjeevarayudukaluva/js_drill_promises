/*
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boardBasedOnId = require("./callback1.cjs");
const dataBasedOnBoardId = require("./callback2.cjs");
const cardsDataBasedOnlistsId = require("./callback3.cjs");

function getThanosMindCards(board, lists, cards, boardId, listName) {
  return new Promise((resolve, reject) => {
    let result = {};
    boardBasedOnId(board, boardId)
      .then((boardData) => {
        //console.log(boardData)
        result["boardData"] = boardData;
        return dataBasedOnBoardId(lists, boardData.id);
      })
      .then((listsData) => {
        result["listsData"] = listsData;
        let mindList = listsData.find((data) => data.name === listName);
        return cardsDataBasedOnlistsId(cards, mindList.id);
      })
      .then((cardsData) => {
        result["cardsData"] = cardsData;
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
module.exports = getThanosMindCards;

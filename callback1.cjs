/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function boardInformation(board, id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const boardBasedOnId = board.find((eachBoard) => id === eachBoard.id);
      resolve(boardBasedOnId);
      reject("id not found ");
    }, 2000);
  });
}

module.exports = boardInformation;

const cards = require("../cards.json");
const boards = require("../boards.json");
const lists = require("../lists_1.json");

const getThanosMindCards = require("../callback4.cjs");
try {
  getThanosMindCards(boards, lists, cards, "mcu453ed", "Mind")
    .then((data) => console.log(data))
    .catch((err) => console.log(err));
} catch (error) {
  console.log(error);
}

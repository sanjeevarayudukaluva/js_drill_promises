/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

function dataBasedOnBoardId(data, id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const listsData = data[id];

      resolve(listsData);

      reject("id not available");
    }, 2000);
  });
}

module.exports = dataBasedOnBoardId;
